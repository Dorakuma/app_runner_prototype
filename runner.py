import argparse
from importlib import import_module
import sys
import sdk

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("task_name")
    parser.add_argument("module_name")
    args, _ = parser.parse_known_args()
    m = import_module(args.module_name)

    sdk.get_event_hub().on("hello_world", lambda: print("Hello World from App Runner"))

    if args.task_name not in sdk.get_hook():
        print("no available hook: " + args.task_name)
    else:
        opt = sdk.get_hook(args.task_name)
        task_args, _ = opt["parser"].parse_known_args()
        opt["func"](**vars(task_args))
