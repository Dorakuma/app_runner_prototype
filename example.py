import sdk
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--test_arg", default="test_arg")


def train(test_arg=""):
    print("test_arg = " + str(test_arg))
    sdk.get_event_hub().emit("hello_world")  # like write_metric


sdk.register_func(train, parser)
