from pyee import EventEmitter
from collections import defaultdict

__ee__ = EventEmitter()
__hook__ = defaultdict(lambda: None)


def get_event_hub():
    return __ee__


def get_hook(k=None):
    if k:
        return __hook__[k]
    else:
        return __hook__


def register_func(func, parser, mode="train"):
    __hook__[mode] = dict(func=func, parser=parser)
